# Vim-Setup

My vim setup on all my current machines.

### NOTE:

In order for the vim directory and vimrc file to function propeyly, place them inside your home/ directory, with a "." preceeding the name of the file and folder.

Example:   

vim/  = .vim  
vimrc = .vimrc  

## Installation of YCM Plugin

The YouCompleteMe Plugin requires to run the __install.py__ script that resides inside the plugin files. Instruction can be found on GitHub as well. 
https://github.com/ycm-core/YouCompleteMe#full-installation-guide
